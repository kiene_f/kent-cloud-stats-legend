<?php
 	header('Access-Control-Allow-Origin: *');  
	require __DIR__ . '/vendor/autoload.php';

	use Silex\Application;
	use Silex\Provider\TwigServiceProvider;
	use Symfony\Component\HttpFoundation\Request;

	$app = new Application();
	$app->register(new TwigServiceProvider());
	$app['twig.path'] = [ __DIR__ ];

	$app->get('/api/champions/{id}', function ($id) use ($app) {
		$memcache = new Memcache;
		if ($memcache->get('champions-'.$id))
		{
			$data = $memcache->get('champions-'.$id);
    		$json = json_encode($data, JSON_PRETTY_PRINT);
    		return ($json);
		}
		else {
    	$db = $app['database'];
	    $twig = $app['twig'];

	    // Stats

	    $sql = 'SELECT SUM(winner)/COUNT(winner)*100 AS winRate, SUM(kills)/COUNT(kills) AS killRate, SUM(deaths)/COUNT(deaths) AS deathRate, SUM(assists)/COUNT(assists) AS assistRate, SUM(doubleKills)/COUNT(kills)*100  AS doubleKills, SUM(tripleKills)/COUNT(kills)*100 AS tripleKills, SUM(quadraKills)/COUNT(kills)*100 AS quadraKills, SUM(pentaKills)/COUNT(kills)*100 AS pentaKills FROM stats.matches WHERE championId = :id';

	    $stm = $db->prepare($sql);
		$stm->execute([
			':id' => $id
			]);
		$champions = $stm->fetchAll();

	    foreach  ($champions as $row) {
	        $winRate = round(floatval($row['winRate']), 2);
			$killRate = round(floatval($row['killRate']), 2);
			$deathRate = round(floatval($row['deathRate']), 2);
			$assistRate = round(floatval($row['assistRate']), 2);
			$doubleKills = round(floatval($row['doubleKills']), 2);
			$tripleKills = round(floatval($row['tripleKills']), 2);
			$quadraKills = round(floatval($row['quadraKills']), 2);
			$pentaKills = round(floatval($row['pentaKills']), 2);
  		}

  		// Spells

  		$sql = 'SELECT spell1Id, count(*) FROM (SELECT spell1Id FROM stats.matches WHERE winner = 1 AND championId = :id UNION ALL SELECT spell2Id FROM stats.matches WHERE winner = 1 AND championId = :id) as spell1Id GROUP BY spell1Id ORDER BY count(*) DESC LIMIT 2';

  		$stm = $db->prepare($sql);
		$stm->execute([
			':id' => $id
			]);
		$spells = $stm->fetchAll();

		foreach  ($spells as $row) {
	        $spellId[] = floatval($row['spell1Id']);
		}

		// Items

		$sql = "SELECT item0, count(*) FROM (SELECT item0 FROM stats.matches WHERE item0 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item1 FROM stats.matches WHERE item1 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item2 FROM stats.matches WHERE item2 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item3 FROM stats.matches WHERE item3 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item4 FROM stats.matches WHERE item4 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item5 FROM stats.matches WHERE item5 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item6 FROM stats.matches WHERE item6 > 0 AND winner = 1 AND championId = :id) as item0 WHERE (item0 > 1399 AND item0 < 1420) OR (item0 = 2045) OR (item0 > 2030 AND item0 < 3286 AND item0 != 3010 AND item0 != 3028 AND item0 != 3034 AND item0 != 3035 AND item0 != 3044 AND item0 != 3052 AND item0 != 3067 AND item0 != 3070 AND item0 != 3073 AND item0 != 3077 AND item0 != 3096 AND item0 != 3097 AND item0 != 3098 AND item0 != 3101 AND item0 != 3105 AND item0 != 3108 AND item0 != 3112 AND item0 != 3113 AND item0 != 3114 AND item0 != 3123 AND item0 != 3133 AND item0 != 3134 AND item0 != 3136 AND item0 != 3140 AND item0 != 3144 AND item0 != 3145 AND item0 != 3155 AND item0 != 3184 AND item0 != 3191 AND item0 != 3196 AND item0 != 3200 AND item0 != 3211 AND item0 != 3222 AND item0 != 3252) OR (item0 = 3742 OR item0 = 3748 OR item0 = 3800 OR item0 = 3812 OR item0 = 3814) GROUP BY item0 ORDER BY count(*) DESC LIMIT 6;";

		$stm = $db->prepare($sql);
		$stm->execute([
			':id' => $id
			]);
		$items = $stm->fetchAll();

		foreach  ($spells as $row) {
	        $items[] = floatval($row['item0']);
		}

		$sql = "SELECT item0, count(*) FROM (SELECT item0 FROM stats.matches WHERE item0 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item1 FROM stats.matches WHERE item1 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item2 FROM stats.matches WHERE item2 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item3 FROM stats.matches WHERE item3 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item4 FROM stats.matches WHERE item4 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item5 FROM stats.matches WHERE item5 > 0 AND winner = 1 AND championId = :id UNION ALL SELECT item6 FROM stats.matches WHERE item6 > 0 AND winner = 1 AND championId = :id) as item0 WHERE (item0 = 3340 OR item0 = 3341 OR item0 = 3361 OR item0 = 3362 OR item0 = 3363 OR item0 = 3364 OR item0 = 3462) GROUP BY item0 ORDER BY count(*) DESC LIMIT 1;";

		$stm = $db->prepare($sql);
		$stm->execute([
			':id' => $id
			]);
		$wards = $stm->fetchAll();

		foreach  ($wards as $row) {
	        $ward = round(floatval($row['item0']), 2);
		}





		$sql = "(SELECT runeId, count(*) FROM (SELECT runeId FROM stats.championsrunes WHERE championId = :id) as runes WHERE (runeId > 5244 AND runeId < 5274) GROUP BY runeId ORDER BY count(*) DESC LIMIT 1) UNION ALL (SELECT runeId, count(*) FROM (SELECT runeId FROM stats.championsrunes WHERE championId = :id) as runes WHERE (runeId > 5274 AND runeId < 5304) GROUP BY runeId ORDER BY count(*) DESC LIMIT 1) UNION ALL (SELECT runeId, count(*) FROM (SELECT runeId FROM stats.championsrunes WHERE championId = :id) as runes WHERE (runeId > 5304 AND runeId < 5333) GROUP BY runeId ORDER BY count(*) DESC LIMIT 1) UNION ALL (SELECT runeId, count(*) FROM (SELECT runeId FROM stats.championsrunes WHERE championId = :id) as runes WHERE (runeId > 5334 AND runeId < 5369) GROUP BY runeId ORDER BY count(*) DESC LIMIT 1)";

		$stm = $db->prepare($sql);
		$stm->execute([
			':id' => $id
			]);
		$marks = $stm->fetchAll();

		foreach  ($marks as $row) {
	        $marks[] = round(floatval($row['runeId']), 2);
		}

	    $results = array(
	    	'id' => $id,
	    	'winRate' => $winRate,
			'killRate' => $killRate,
			'deathRate' => $deathRate,
			'assistRate' => $assistRate,
			'doubleKills' => $doubleKills,
			'tripleKills' => $tripleKills,
			'quadraKills' => $quadraKills,
			'pentaKills' => $pentaKills,
			'kda' => round((($killRate + $assistRate) / $deathRate), 2),
			'spells' => array(
				'spell1Id' => $spellId[0],
				'spell2Id' => $spellId[1]
			),
			'items' => array(
				'item0' => floatval($items[0][0]),
				'item1' => floatval($items[1][0]),
				'item2' => floatval($items[2][0]),
				'item3' => floatval($items[3][0]),
				'item4' => floatval($items[4][0]),
				'item5' => floatval($items[5][0]),
				'item6' => floatval($ward)
			),
			'runes' => array(
				'mark' => floatval($marks[0][0]),
				'glyph' => floatval($marks[1][0]),
				'seal' => floatval($marks[2][0]),
				'quint' => floatval($marks[3][0])
			)
	    );

	    $memcache->set('champions-'.$id, $results);
	    $json = json_encode($results, JSON_PRETTY_PRINT);

	    return $json;
	}
	});



	$app['database'] = function () use ($app) {
	    $dsn = getenv('MYSQL_DSN');
	    $user = getenv('MYSQL_USER');
	    $password = getenv('MYSQL_PASSWORD');
	    if (!isset($dsn, $user) || false === $password) {
	        throw new Exception('Set MYSQL_DSN, MYSQL_USER, and MYSQL_PASSWORD environment variables');
	    }

	    $db = new PDO($dsn, $user, $password);
	    return $db;
	};

	$app->run();
?>