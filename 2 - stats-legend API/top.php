<?php
 	header('Access-Control-Allow-Origin: *');  
	require __DIR__ . '/vendor/autoload.php';

	use Silex\Application;
	use Silex\Provider\TwigServiceProvider;
	use Symfony\Component\HttpFoundation\Request;

	// create the Silex application
	$app = new Application();
	$app->register(new TwigServiceProvider());
	$app['twig.path'] = [ __DIR__ ];

	$app->get('/api/top', function () use ($app) {
		$memcache = new Memcache;
		if ($memcache->get('top'))
		{
			$data = $memcache->get('top');
    		$json = json_encode($data, JSON_PRETTY_PRINT);
    		return ($json);
		}
		else {
			$db = $app['database'];
		    $twig = $app['twig'];
		    // Stats
		    $sql = 'SELECT championId, SUM(winner)/COUNT(winner)*100 AS win FROM stats.matches GROUP BY championId ORDER BY win DESC LIMIT 50';
		    $stm = $db->prepare($sql);
		    $stm->execute();
		    $results = $stm->fetchAll();
		    $top = array();
		    foreach ($results as $key => $val) {
		    	$top[] = array('championId' => $val['championId'], 'win' => round(floatval($val['win']), 2));
		    }
		    $memcache->set('top', $top);
		    return (json_encode($top, JSON_PRETTY_PRINT));
		}
	});


	$app['database'] = function () use ($app) {
	    $dsn = getenv('MYSQL_DSN');
	    $user = getenv('MYSQL_USER');
	    $password = getenv('MYSQL_PASSWORD');
	    if (!isset($dsn, $user) || false === $password) {
	        throw new Exception('Set MYSQL_DSN, MYSQL_USER, and MYSQL_PASSWORD environment variables');
	    }

	    $db = new PDO($dsn, $user, $password);
	    return $db;
	};

	$app->run();
?>