# Kent - Cloud - Stats Legend #
### French

Site réalisé à 3, utilisant plusieurs technologies offert par le Cloud afin de récupérer et d'analyser de grandes quantités de données de partie du jeu "League of legends". Ces données permettent de produire un tableau récapitulatif pour chaque champion des meilleurs objets, sorts, runes qui ont le plus de chance de faire gagner le joueur. (CO846 Cloud Computing)

![alt text](http://francois.kiene.fr/assets/images/works/annie.jpg)

## Built With

* Google compute engine (SaaS)
* Google app engine (PaaS)
* Google cloud SQL (DaaS)
* PHP, Symfony, Silex, AngularJS
