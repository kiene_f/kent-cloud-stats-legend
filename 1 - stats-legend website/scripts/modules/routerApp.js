var routerApp = angular.module('routerApp', ['ui.router', 'angularCSS', 'app']);
 
routerApp.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('login');
    $stateProvider
        .state('login', {
            url: '/login',
            controller: 'login',
            data:{ pageTitle: 'Login' },
            templateUrl: '/views/login.html'
        })
        .state('content', {
            abstract: true,
            templateUrl: '/views/content.html'
        })
        .state('home', {
            url: '/home',
            parent: 'content',
            controller: 'home',
            data:{ pageTitle: 'Home' },
            templateUrl: '/views/champions.html'
        })
        .state('champion', {
            url: '/champion/{id:int}',
            parent: 'content',
            controller: 'champion',
            data:{ pageTitle: 'Champion' },
            templateUrl: '/views/champion.html'
        })
        .state('tier', {
            url: '/tier',
            parent: 'content',
            controller: 'tier',
            data:{ pageTitle: 'Tier List' },
            templateUrl: '/views/tier.html'
        })
});