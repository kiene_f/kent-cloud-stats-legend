angular.module('app', ['routerApp'])
.controller('home', function($scope, $state, $rootScope, champions) {
	if (!$rootScope.isLogin)
		$state.go('login');
	$rootScope.pageTitle = $state.current.data.pageTitle;
	champions.list().then(function(response) {
		$scope.items = response;
	});
})
.controller('login', function($scope, $state, $rootScope, champions) {
	$rootScope.reload = function()
	{
	   location.reload(); 
	}
	$scope.submit = function () {
		if ($scope.inputEmail == "basic" && $scope.inputPassword == "secret")
		{
			$rootScope.isLogin = true;
			$rootScope.userName = "Basic";
			$state.go('home');
		}
		else if ($scope.inputEmail == "premium" && $scope.inputPassword == "secret")
		{
			$rootScope.isLogin = true;
			$rootScope.isPremium = true;
			$rootScope.userName = "Premium";
			$state.go('home');
		}
	}

})
.controller('tier', function($scope, $state, $rootScope, champions, top, championById) {
	if (!$rootScope.isLogin)
		$state.go('login');
	$rootScope.pageTitle = $state.current.data.pageTitle;
	top.list().then(function(response) {
		$scope.tier = response;
		$scope.champions = [];
		angular.forEach(response, function(champion, key) {
			championById.list(champion.championId).then(function(response) {
				$scope.champions.push(response);
				console.log(response);
			});
		});
	});
})
.controller('champion', function($scope, $stateParams, $state, $rootScope, champions, stats, spells, runes) {
	if (!$rootScope.isLogin)
		$state.go('login');
	var id = $stateParams.id;
	champions.list().then(function(response) {
		$rootScope.pageTitle = $state.current.data.pageTitle;
		angular.forEach(response, function(item, key) {
			if (item.key == id)
				$scope.item = item;
		});
		$scope.masteries = "/views/masteries/"+$scope.item.tags[0]+".html";
		stats.list(id).then(function(response) {
			$scope.stats = response;
			spells.list().then(function(response) {
				angular.forEach(response, function(item, key) {
					if (item.key == $scope.stats.spells.spell1Id)
						$scope.spell0 = item;
					else if (item.key == $scope.stats.spells.spell2Id)
						$scope.spell1 = item;
				});
			});
			runes.list().then(function(response) {
				angular.forEach(response, function(item, key) {
					if (key.indexOf($scope.stats.runes.mark) != -1)
						$scope.mark = item;
					else if (key.indexOf($scope.stats.runes.glyph) != -1)
						$scope.glyph = item;
					else if (key.indexOf($scope.stats.runes.seal) != -1)
						$scope.seal = item;
					else if (key.indexOf($scope.stats.runes.quint) != -1)
						$scope.quint = item;
				});
				$('[data-toggle="tooltip"]').tooltip();
			});
		});
	});
})
.factory('champions', function($http, $q) {
    var items = [];
    return {
        list: function() {
        	var deferred = $q.defer();
            if (items.length == 0) {    // items array is empty so populate it and return list from server to controller
                $http.get("http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json").then(function(response) {
                    items = response.data.data;
                    deferred.resolve(response.data.data);
                });
            } else {
                deferred.resolve(items);
            }
            return deferred.promise;   // items exist already so just return the array
        }
    }
})
.factory('championById', function($http, $q) {
	var items = [];
    return {
        list: function(id) {
        	var deferred = $q.defer();
            if (items.length == 0) {    // items array is empty so populate it and return list from server to controller
                $http.get("http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json").then(function(response) {
                    angular.forEach(response.data.data, function(item, key) {
						if (item.key == id)
							items = item;
					});
                    deferred.resolve(items);
                });
            } else {
                deferred.resolve(items);
            }
            return deferred.promise;   // items exist already so just return the array
        }
    }
})
.factory('stats', function($http, $q) {
    var items = [];
    var idSave = 0;
    return {
        list: function(id) {
        	var deferred = $q.defer();
            if (items.length == 0 || id != idSave) {    // items array is empty so populate it and return list from server to controller
                $http.get("https://stats-legend.appspot.com/api/champions/"+id).then(function(response) {
                    items = response.data;
                    idSave = id;
                    deferred.resolve(response.data);
                });
            } else {
                deferred.resolve(items);
            }
            return deferred.promise;   // items exist already so just return the array
        }
    }
})
.factory('spells', function($http, $q) {
    var items = [];
    return {
        list: function() {
        	var deferred = $q.defer();
            if (items.length == 0) {    // items array is empty so populate it and return list from server to controller
                $http.get("http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/summoner.json").then(function(response) {
                    items = response.data.data;
                    deferred.resolve(response.data.data);
                });
            } else {
                deferred.resolve(items);
            }
            return deferred.promise;   // items exist already so just return the array
        }
    }
})
.factory('runes', function($http, $q) {
    var items = [];
    return {
        list: function() {
        	var deferred = $q.defer();
            if (items.length == 0) {    // items array is empty so populate it and return list from server to controller
                $http.get("http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/rune.json").then(function(response) {
                    items = response.data.data;
                    deferred.resolve(response.data.data);
                });
            } else {
                deferred.resolve(items);
            }
            return deferred.promise;   // items exist already so just return the array
        }
    }
})
.factory('top', function($http, $q) {
    var items = [];
    return {
        list: function() {
        	var deferred = $q.defer();
            if (items.length == 0) {    // items array is empty so populate it and return list from server to controller
                $http.get("http://stats-legend.appspot.com/api/top").then(function(response) {
                    items = response.data;
                    deferred.resolve(response.data);
                });
            } else {
                deferred.resolve(items);
            }
            return deferred.promise;   // items exist already so just return the array
        }
    }
});


